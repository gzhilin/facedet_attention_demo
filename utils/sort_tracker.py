import numpy as np
from scipy.optimize import linear_sum_assignment
from filterpy.kalman import KalmanFilter

def corners_to_shape(corners):
    w, h = corners[2:] - corners[:2]
    x, y = (corners[2:] + corners[:2])/2
    s = w*h
    r = w/h
    return np.array([x,y,s,r])

def shape_to_corners(shape):
    x,y,s,r = shape
    w = np.sqrt(s*r)
    h = s/w
    return np.array([x - w/2, y - h/2, x + w/2, y + h/2])

class KalmanBoxTracker():
    def __init__(self, bbox):
        self.kf = KalmanFilter(dim_x=7, dim_z=4)

        self.kf.F = np.array([[1,0,0,0,1,0,0],
                              [0,1,0,0,0,1,0],
                              [0,0,1,0,0,0,1],
                              [0,0,0,1,0,0,0],
                              [0,0,0,0,1,0,0],
                              [0,0,0,0,0,1,0],
                              [0,0,0,0,0,0,1]])

        self.kf.H = np.array([[1,0,0,0,0,0,0],
                              [0,1,0,0,0,0,0],
                              [0,0,1,0,0,0,0],
                              [0,0,0,1,0,0,0]])

        self.kf.R[2:,2:] *= 10.
        self.kf.P[4:,4:] *= 1000. #give high uncertainty to the unobservable initial velocities
        self.kf.P *= 10.
        self.kf.Q[-1,-1] *= 0.01
        self.kf.Q[4:,4:] *= 0.01

        self.kf.x[:4] = corners_to_shape(bbox).reshape((4,1))
        self.time_since_update = 0
        self.hits = 0
        self.hit_streak = 0
        self.age = 0
        self.history = []

    def tock(self, bbox=None):
        if bbox is None:
            return
        self.time_since_update = 0
        self.hits += 1
        self.hit_streak += 1
        self.kf.update(corners_to_shape(bbox).reshape((4,1)))

    def tick(self):
        if ((self.kf.x[6]+self.kf.x[2])<=0):
            self.kf.x[6] *= 0
        self.kf.predict()
        self.age += 1
        if (self.time_since_update>0):
            self.hit_streak = 0
        self.time_since_update += 1
        return shape_to_corners(self.kf.x[:4,0])

    def get_state(self):
        return shape_to_corners(self.kf.x[:4,0])

def iou(boxes_a, boxes_b):
    boxes_a = boxes_a[:,None,...]
    boxes_b = boxes_b[None,...]
    intersections =  np.maximum(0,
            np.minimum(boxes_a[...,2],boxes_b[...,2])-np.maximum(boxes_a[...,0],boxes_b[...,0]))\
                    *np.maximum(0,
            np.minimum(boxes_a[...,3],boxes_b[...,3])-np.maximum(boxes_a[...,1],boxes_b[...,1]))
    unions =  np.maximum(0, boxes_a[...,2]-boxes_a[...,0])\
             *np.maximum(0, boxes_a[...,3]-boxes_a[...,1])\
             +np.maximum(0, boxes_b[...,2]-boxes_b[...,0])\
             *np.maximum(0, boxes_b[...,3]-boxes_b[...,1])\
             -intersections
    return intersections/(unions + 1e-10)

class SortTracker():
    def __init__(self, max_time_since_update=0, iou_threshold=0.3):
        self.max_time_since_update = max_time_since_update
        self.iou_threshold = iou_threshold
        self.trackers = np.array([])
        self.ids = np.array([])
        self.frame_count = 0
        self.tracker_count = 0

    def update(self, bbox_dets=np.empty((0,4))):
        self.frame_count += 1
        try:
            bbox_preds = np.stack([tracker.tick() for tracker in self.trackers])
        except:
            bbox_preds = np.empty((0,4))

        iou_matrix = iou(bbox_dets, bbox_preds)
        matched_dets, matched_preds = linear_sum_assignment(iou_matrix,
                                                            maximize=True)

        above_iou_threshold = iou_matrix[matched_dets, matched_preds] > self.iou_threshold
        matched_dets = matched_dets[above_iou_threshold]
        matched_preds = matched_preds[above_iou_threshold]

        unmatched_dets = np.delete(np.arange(len(bbox_dets)), matched_dets)
        unmatched_preds = np.delete(np.arange(len(bbox_preds)), matched_preds)

        living_trackers = []
        living_ids = []
        for tracker, idx, bbox in zip(self.trackers[matched_preds],
                                      self.ids[matched_preds],
                                      bbox_dets[matched_dets]):
            tracker.tock(bbox)
            living_trackers.append(tracker)
            living_ids.append(idx)


        new_trackers = []
        new_ids = []
        for bbox in bbox_dets[unmatched_dets]:
            new_tracker = KalmanBoxTracker(bbox)
            new_id = self.tracker_count
            self.tracker_count += 1
            new_trackers.append(new_tracker)
            new_ids.append(new_id)

        dets_ids = np.empty(len(bbox_dets), dtype=np.int)
        dets_ids[matched_dets] = np.array(living_ids)
        dets_ids[unmatched_dets] = np.array(new_ids)

        dead_ids = []
        for tracker, idx in zip(self.trackers[unmatched_preds],
                                self.ids[unmatched_preds]):
            if tracker.time_since_update > self.max_time_since_update:
                dead_ids.append(idx)
            else:
                living_trackers.append(tracker)
                living_ids.append(idx)

        dead_ids = np.array(dead_ids, dtype=np.int)

        self.trackers = np.array(living_trackers + new_trackers)
        self.ids = np.array(living_ids + new_ids, dtype=np.int)

        return {
            "tracks_matched_to": dets_ids,
            "dead_tracks": dead_ids
        }

    def predict_bboxes(self):
        bbox_preds = [tracker.get_state() for tracker in self.trackers]
        bbox_preds = np.stack(bbox_preds) if len(bbox_preds) > 0 else np.empty((0,4))
        return bbox_preds
