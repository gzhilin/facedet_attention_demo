import cv2
import numpy as np
from collections import deque

from .glance import Glance


class FrameGenerator:

    def __init__(self, source, target_fps=10):
        self.source = source
        self.target_fps = target_fps
        self._cap = cv2.VideoCapture(self.source)
        self._presenting_rate = int(round(0.01 + self._cap.get(cv2.CAP_PROP_FPS)/self.target_fps))
        self._frame_counter = 0

    @property
    def fps(self):
        return self._cap.get(cv2.CAP_PROP_FPS)/self._presenting_rate

    @property
    def frame_size(self):
        return (
            int(self._cap.get(cv2.CAP_PROP_FRAME_WIDTH)),
            int(self._cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        )

    def __iter__(self):
        counter = 0
        while True:
            ret, frame = self._cap.read()
            if not ret:
                break
            if counter == 0:
                frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                yield frame
            counter = (counter + 1) % self._presenting_rate
        self._cap.release()

    def release(self):
        self._cap.release()


class BackgroundSubtractorMOG2:

    def __init__(self, downsampling_factor):
        self.downsampling_factor = downsampling_factor
        self._bg_sub = cv2.createBackgroundSubtractorMOG2()
        self._frame_size = None
        self._segmap_size = None

    def __call__(self, frame):
        if not self._frame_size:
            self._frame_size = frame.shape[:2][::-1]
            self._segmap_size = tuple(side//self.downsampling_factor for side in self._frame_size)
        assert frame.shape[:2][::-1] == self._frame_size

        frame = cv2.resize(frame, self._segmap_size)
        segmap = self._bg_sub.apply(frame)
        segmap[segmap < 255] = 0
        segmap = cv2.medianBlur(segmap, 3)
        return segmap


class MotionGlanceGenerator:

    def __init__(self, target_glance_size, glance_downsampling_factor, segmap_downsampling_factor, deactivation_time=5):
        self.target_glance_size = target_glance_size
        self.glance_downsampling_factor = glance_downsampling_factor
        self.segmap_downsampling_factor = segmap_downsampling_factor
        self.deactivation_time = deactivation_time
        self._bg_sub = BackgroundSubtractorMOG2(segmap_downsampling_factor)
        self._seg_masks = deque([None]*deactivation_time, maxlen=deactivation_time)
        self._last_segmap = None
        self._last_segmap_masked = None

    def __call__(self, frame):
        segmap = self._bg_sub(frame)
        self._last_segmap = segmap.copy()

        for seg_mask in self._seg_masks:
            if seg_mask is not None:
                x_min, y_min, x_max, y_max = np.maximum(0, seg_mask)
                segmap[y_min:y_max, x_min:x_max] = 128
        self._last_segmap_masked = segmap.copy()

        active_pixels = np.stack(np.where(segmap == 255)[::-1], axis=-1)
        if len(active_pixels) == 0:
            self._seg_masks.append(None)
            return None

        pivot = active_pixels[np.random.randint(0, len(active_pixels))]
        pivot = pivot * (np.array(frame.shape[:2])/np.array(segmap.shape))[::-1]
        sides = np.array(self.target_glance_size)*self.glance_downsampling_factor
        glance_area = np.concatenate([pivot - sides/2, pivot + sides/2]).astype(np.int)
        glance_area = _fit_area(glance_area, frame.shape[1], frame.shape[0])

        seg_mask = (np.array(glance_area)/self.segmap_downsampling_factor).astype(np.int)
        self._seg_masks.append(seg_mask)

        glance = Glance(
            cv2.resize(_crop(frame, glance_area), self.target_glance_size),
            glance_area
        )
        return glance

    def get_last_segmap(self, masked=True):
        out = self._last_segmap_masked if masked else self._last_segmap
        return out


class ObjectGlanceGenerator:

    def __init__(self, target_glance_size, expansion_factor):
        self.target_glance_size = target_glance_size
        self.expansion_factor = expansion_factor

    def __call__(self, frame, bboxes):
        bboxes = np.array(bboxes)
        centers = 0.5*(bboxes[:, :2] + bboxes[:, 2:])
        sizes = self.expansion_factor*(bboxes[:, 2:] - bboxes[:, :2]).max(axis=-1, keepdims=True)
        crop_areas = np.concatenate(
            [centers - sizes/2, centers + sizes/2],
            axis=-1
        ).astype(np.int)

        glances = []
        for crop_area in crop_areas:
            if crop_area[2]-crop_area[0] < 10 or crop_area[3]-crop_area[1] < 10:
                continue
            glance = Glance(
                cv2.resize(_crop(frame, crop_area), self.target_glance_size),
                crop_area
            )
            glances.append(glance)
        return glances


def _crop(image, crop_area):
    x_min, y_min, x_max, y_max = (int(x) for x in crop_area)
    x_min, left_pad = max(0, x_min), max(0, -x_min)
    y_min, top_pad = max(0, y_min), max(0, -y_min)
    x_max, right_pad = min(image.shape[1], x_max), max(0, x_max - image.shape[1])
    y_max, bottom_pad = min(image.shape[0], y_max), max(0, y_max - image.shape[0])
    crop = cv2.copyMakeBorder(
        image[y_min:y_max, x_min:x_max],
        top_pad, bottom_pad, left_pad, right_pad,
        cv2.BORDER_CONSTANT
    )
    return crop


def _fit_area(area, width, height):
    x_min, y_min, x_max, y_max = area
    x_min, x_max = _fit_side(x_min, x_max, width)
    y_min, y_max = _fit_side(y_min, y_max, height)
    return x_min, y_min, x_max, y_max


def _fit_side(c_min, c_max, size):
    if c_max - c_min > size:
        diff = size - (c_max - c_min)
        c_min = - diff/2
        c_max = size + diff/2
    if c_min < 0:
        c_min, c_max = 0, c_max - c_min
    if c_max > size:
        c_min, c_max = c_min - c_max + size, size
    return c_min, c_max
