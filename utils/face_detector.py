import torch

import numpy as np

from .torch_engine_wrapper import TorchEngineWrapper
from .torch_postprocessor import Postprocessor
from .objects import FaceWithOrientation


class FaceDetector:

    def __init__(self, engine_path):
        self.core = TorchEngineWrapper.from_file(engine_path)
        self.pp = Postprocessor()

    def __call__(self, images):
        if len(images) == 0:
            return []

        inputs = torch.tensor(np.stack(images, axis=0).transpose(0, 3, 1, 2)/255).half().cuda()
        batch_size = len(images)
        max_batch_size = self.core.max_batch_size

        ind_b, centers, quaternions, scores = [], [], [], []
        for ind in range((batch_size-1)//max_batch_size + 1):
            res = self.pp(**self.core(inputs[ind*max_batch_size:(ind+1)*max_batch_size]))
            ind_b.append(res["ind_b"])
            centers.append(res["centers"])
            quaternions.append(res["quaternions"])
            scores.append(res["scores"])
        ind_b, centers, quaternions, scores = (np.concatenate(x) for x in [ind_b, centers, quaternions, scores])
        faces = [
            FaceWithOrientation(center, quat, score)
            for center, quat, score in zip(centers, quaternions, scores)
        ]

        inds, counts = np.unique(ind_b, return_counts=True)
        splits = np.zeros((batch_size,), dtype=np.int)
        splits[inds] = counts

        faces = np.split(faces, splits.cumsum()[:-1])
        return faces
