import numpy as np
import cv2
import copy
import os


class Object:

    def __init__(self, cls, bbox, score):
        self.cls = cls
        self.bbox = bbox
        self.score = score

    def translate(self, dx, dy):
        new = copy.copy(self)
        x_min, y_min, x_max, y_max = new.bbox
        new.bbox = (x_min + dx, y_min + dy, x_max + dx, y_max + dy)
        return new

    def resize(self, fx, fy):
        new = copy.copy(self)
        x_min, y_min, x_max, y_max = new.bbox
        new.bbox = (x_min*fx, y_min*fy, x_max*fx, y_max*fy)
        return new


class FaceWithOrientation(Object):
    reference_scale = 64 #magic number for making a face crop
    crop_size = 112
    facial_keypoints_3D = np.load(os.path.join(os.path.dirname(__file__), "facial_keypoints_3D.npy"))
    _levi_civita_symbol = np.load(os.path.join(os.path.dirname(__file__), "levi_civita_symbol.npy"))

    def __init__(self, center, quaternions, score):
        keypoints, crop_center = self._from_3d_keypoints(center, quaternions)
        bbox = self._bbox_from_keypoints(keypoints)
        super().__init__("face", bbox, score)
        self.center = center
        self.crop_center = crop_center
        self.quaternions = quaternions
        self.keypoints = keypoints

    def _from_3d_keypoints(self, center, quaternions):
        u0 = quaternions[0]
        uv = np.array(quaternions[1:])

        I = np.diag(np.ones((3,)))*(u0*u0 - (uv*uv).sum())
        II = 2*uv[:, None]*uv[None, :]
        III = 2*u0*(self._levi_civita_symbol @ uv)
        mat = I + II + III

        keypoints = (self.facial_keypoints_3D @ mat.T)[:, :2]
        keypoints = keypoints + center
        crop_center = mat @ (0, 0, self.facial_keypoints_3D[:, 2].mean(axis=0))
        crop_center = center + crop_center[:2]*0.5
        return keypoints, crop_center

    @staticmethod
    def _bbox_from_keypoints(keypoints):
        x_min, y_min = keypoints.min(axis=0)
        x_max, y_max = keypoints.max(axis=0)
        return (x_min, y_min, x_max, y_max)

    def make_crop(self, image):
        u0 = self.quaternions[0]
        u3 = self.quaternions[3]
        scale_correction = sum([q*q for q in self.quaternions])/(u0*u0 + u3*u3)
        mat = np.array([
            [u0*u0 - u3*u3,  2*u0*u3],
            [-2*u0*u3, u0*u0 - u3*u3]
        ])*scale_correction/self.reference_scale
        mat = np.linalg.inv(mat)
        shift = self.crop_center
        shift = (self.crop_size/2,)*2 - shift @ mat.T
        affine_warp = np.concatenate([mat, shift[:, None]], axis=1)
        crop = cv2.warpAffine(image, affine_warp, (self.crop_size,)*2)
        self.crop = crop
        return crop

    def translate(self, dx, dy):
        new = super().translate(dx, dy)
        new.center = (new.center[0] + dx, new.center[1] + dy)
        new.crop_center = (new.crop_center[0] + dx, new.crop_center[1] + dy)
        new.keypoints = new.keypoints + (dx, dy)
        return new

    def resize(self, fx, fy):
        new = super().resize(fx, fy)
        new.center = (new.center[0]*fx, new.center[1]*fy)
        new.crop_center = (new.crop_center[0]*fx, new.crop_center[1]*fy)
        new.quaternions = tuple(q*(fx*fy)**(1/4) for q in new.quaternions)
        new.keypoints = new.keypoints*(fx, fy)
        return new
