
class Glance:

    def __init__(self, image, source_area):
        self.image = image
        self.source_area = source_area
        self.objects = None

    @property
    def image_size(self):
        height, width = self.image.shape[:2]
        return width, height

    def set_detections(self, objects):
        assert self.objects is None
        self.objects = objects

    def get_detections(self):
        x_min, y_min, x_max, y_max = self.source_area
        width, height = self.image_size

        dx, dy = x_min, y_min
        fx, fy = (x_max - x_min)/width, (y_max - y_min)/height

        objects = [obj.resize(fx, fy).translate(dx, dy) for obj in self.objects]
        return objects
