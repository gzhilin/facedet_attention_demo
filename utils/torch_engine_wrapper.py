import torch
from torch import nn
import tensorrt as trt


class TorchEngineWrapper(nn.Module):

    def __init__(self, engine):
        super().__init__()
        self.engine = engine
        self.context = self.engine.create_execution_context()

    def forward(self, *inputs):
        input_binding_inds = [
            ind for ind in range(self.engine.num_bindings)
            if self.engine.binding_is_input(ind)
        ]
        output_binding_inds = [
            ind for ind in range(self.engine.num_bindings)
            if not self.engine.binding_is_input(ind)
        ]
        assert len(input_binding_inds) == len(inputs)
        batch_size = inputs[0].shape[0]
        assert batch_size < self.engine.max_batch_size

        bindings = [None for _ in range(self.engine.num_bindings)]

        for ind, inp in zip(input_binding_inds, inputs):
            dtype = self._torch_dtype_from_trt(self.engine.get_binding_dtype(ind))
            assert inp.dtype == dtype
            shape = (batch_size,) + tuple(self.engine.get_binding_shape(ind))[1:]
            assert shape == inp.shape
            #device = self._torch_device_from_trt(self.engine.get_location(ind))
            #assert inp.device == device
            bindings[ind] = inp.contiguous().data_ptr()
            self.context.set_binding_shape(ind, shape)

        outputs = []
        for ind in output_binding_inds:
            dtype = self._torch_dtype_from_trt(self.engine.get_binding_dtype(ind))
            shape = (batch_size,) + tuple(self.engine.get_binding_shape(ind))[1:]
            device = self._torch_device_from_trt(self.engine.get_location(ind))
            output = torch.empty(size=shape, dtype=dtype, device=device)
            outputs.append(output)
            bindings[ind] = output.data_ptr()

        self.context.execute_async(
            batch_size, bindings, torch.cuda.current_stream().cuda_stream
        )

        outputs = tuple(outputs)
        if len(outputs) == 1:
            outputs = outputs[0]
        else:
            outputs = dict(zip(self.output_names, outputs))
        return outputs

    @staticmethod
    def _torch_dtype_from_trt(dtype):
        if dtype == trt.int8:
            return torch.int8
        elif trt.__version__ >= '7.0' and dtype == trt.bool:
            return torch.bool
        elif dtype == trt.int32:
            return torch.int32
        elif dtype == trt.float16:
            return torch.float16
        elif dtype == trt.float32:
            return torch.float32
        else:
            raise TypeError("%s is not supported by torch" % dtype)

    @staticmethod
    def _torch_device_from_trt(device):
        if device == trt.TensorLocation.DEVICE:
            return torch.device("cuda")
        elif device == trt.TensorLocation.HOST:
            return torch.device("cpu")
        else:
            return TypeError("%s is not supported by torch" % device)

    def enable_profiling(self):
        if not self.context.profiler:
            self.context.profiler = trt.Profiler()

    @classmethod
    def from_file(cls, file_path):
        with trt.Logger() as logger, trt.Runtime(logger) as runtime,\
                open(file_path, "rb") as fp:
            engine = runtime.deserialize_cuda_engine(fp.read())
        return cls(engine)

    @property
    def _input_binding_inds(self):
        input_binding_inds = [
            ind for ind in range(self.engine.num_bindings)
            if self.engine.binding_is_input(ind)
        ]
        return input_binding_inds

    @property
    def _output_binding_inds(self):
        output_binding_inds = [
            ind for ind in range(self.engine.num_bindings)
            if not self.engine.binding_is_input(ind)
        ]
        return output_binding_inds

    @property
    def input_dtypes(self):
        input_dtypes = tuple(
            self._torch_dtype_from_trt(self.engine.get_binding_dtype(ind))
            for ind in self._input_binding_inds
        )
        return input_dtypes

    @property
    def output_dtypes(self):
        output_dtypes = tuple(
            self._torch_dtype_from_trt(self.engine.get_binding_dtype(ind))
            for ind in self._output_binding_inds
        )
        return output_dtypes

    @property
    def input_names(self):
        input_names = tuple(
            self.engine.get_binding_name(ind)
            for ind in self._input_binding_inds
        )
        return input_names

    @property
    def output_names(self):
        output_names = tuple(
            self.engine.get_binding_name(ind)
            for ind in self._output_binding_inds
        )
        return output_names

    @property
    def input_shapes(self):
        input_shapes = tuple(
            self.engine.get_binding_shape(ind)
            for ind in self._input_binding_inds
        )
        return input_shapes

    @property
    def max_batch_size(self):
        return self.engine.max_batch_size
