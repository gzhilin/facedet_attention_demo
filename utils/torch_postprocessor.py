import torch
from torch import nn
from torch.nn import functional as F

import numpy as np


class Postprocessor(nn.Module):
    
    def __init__(self, score_threshold=0.5):
        super().__init__()
        self.score_threshold = score_threshold
    
    def forward(self, *inputs, **named_inputs):
        if "logits" in named_inputs.keys():
            logits = named_inputs.pop("logits")
        else:
            logits = inputs[0]
            inputs = inputs[1:]
            
        local_maxima = F.max_pool2d(logits, kernel_size=3, stride=1, padding=1) == logits
        above_threshold = logits > np.log(self.score_threshold/(1 - self.score_threshold))
        ind_b, ind_c, ind_h, ind_w = torch.where(local_maxima & above_threshold)
        
        scores = torch.sigmoid(logits[ind_b, ind_c, ind_h, ind_w]).cpu().numpy()
        
        outputs = tuple(inp[ind_b, ..., ind_h, ind_w].cpu().numpy() for inp in inputs)
        named_outputs = {
            name: inp[ind_b, ..., ind_h, ind_w].cpu().numpy()
            for name, inp in named_inputs.items()
        }
        if len(outputs) == 0:
            result = {
                "ind_b": ind_b.cpu().numpy(),
                "scores": scores,
                **named_outputs
            }
        else:
            result = (ind_b.cpu(), scores) + outputs
            if len(named_outputs) > 0:
                result = result + (named_outputs, )
        return result
