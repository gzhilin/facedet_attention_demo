import numpy as np
from .sort_tracker import SortTracker
from collections import defaultdict

from .frame_generation import ObjectGlanceGenerator


class FaceTrackManager:

    def __init__(self, target_glance_size, expansion_factor, endpoint, iou_threshold=0.3):
        self.glance_generator = ObjectGlanceGenerator(target_glance_size, expansion_factor)
        self.endpoint = endpoint
        self.tracker = SortTracker(max_time_since_update=1, iou_threshold=iou_threshold)
        self.tracks = defaultdict(Track)
        self.frame_counter = 0

    def update(self, objects):
        self.frame_counter += 1
        objects = [obj for obj in objects if obj.cls=="face"]
        tracking_result = self.tracker.update(
            np.array([obj.bbox for obj in objects]) if objects else np.empty((0, 4))
        )
        for idx, obj in zip(tracking_result["tracks_matched_to"], objects):
            track = self.tracks[idx]
            track.add(obj)
        for idx in tracking_result["dead_tracks"]:
            #track = self.tracks.pop(idx)
            self._finalize(track)

    def _finalize(self, track):
        pass

    def generate_glances(self, frame):
        bboxes = self.tracker.predict_bboxes()
        glances = self.glance_generator(frame, bboxes)
        return glances


class Track:

    def __init__(self):
        self.history = []

    def add(self, obj):
        self.history.append(obj)
