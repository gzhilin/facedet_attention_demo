import tensorrt as trt


def create_engine(image_size, max_batch_size, onnx_path, save_path):
    logger = trt.Logger()
    builder = trt.Builder(logger)

    network = builder.create_network(
        1 << int(trt.NetworkDefinitionCreationFlag.EXPLICIT_BATCH)
    )

    parser = trt.OnnxParser(network, logger)
    with open(onnx_path, "rb") as model_fp:
        parser.parse(model_fp.read())
    network.get_input(0).dtype = trt.DataType.HALF

    builder.max_batch_size = max_batch_size
    config = builder.create_builder_config()
    config.max_workspace_size = 1 << 30
    config.set_flag(trt.BuilderFlag.FP16)

    opt_profile = builder.create_optimization_profile()
    opt_profile.set_shape(
        "image",
        min=(1, 3, *image_size[::-1]),
        opt=(max_batch_size, 3, *image_size[::-1]),
        max=(max_batch_size, 3, *image_size[::-1]),
    )
    config.add_optimization_profile(opt_profile)

    engine = builder.build_engine(network, config)
    with open(save_path, "wb") as fp:
        fp.write(engine.serialize())


if __name__ == "__main__":
    create_engine((417, 417), 32, "resnet18_facepose_v3.onnx", "resnet18_facepose_v3_417x417.engine")
    create_engine((97, 97), 256, "resnet18_facepose_v3.onnx", "resnet18_facepose_v3_97x97.engine")
