import torch
from torch import nn
from torch.nn import functional as F

from torchvision.ops import nms

import cv2
import numpy as np

import argparse
from utils.frame_generation import FrameGenerator, MotionGlanceGenerator
from utils.track_manager import FaceTrackManager
from utils.face_detector import FaceDetector


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--source", help="Video URL/filepath")
    parser.add_argument("-o", "--output", help="Processed video path", default="out.avi")
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()

    large_detector = FaceDetector("resnet18_facepose_v3_417x417.engine")
    small_detector = FaceDetector("resnet18_facepose_v3_97x97.engine")

    track_manager = FaceTrackManager(
        target_glance_size=(97, 97),
        expansion_factor=2.5,
        endpoint=None,
        iou_threshold=0.1,
    )
    motion_glance_generator = MotionGlanceGenerator(
        target_glance_size=(417, 417),
        glance_downsampling_factor=1.5,
        segmap_downsampling_factor=8,
        deactivation_time=5,
    )
    frame_generator = FrameGenerator(args.source, target_fps=10)

    video_writer = cv2.VideoWriter(
        args.output,
        cv2.VideoWriter_fourcc(*"MJPG"),
        frame_generator.fps,
        (frame_generator.frame_size[0], 2*frame_generator.frame_size[1]),
    )

    for frame in frame_generator:
        large_glance = motion_glance_generator(frame)
        small_glances = track_manager.generate_glances(frame)

        faces = []
        small_glance_faces = small_detector([glance.image for glance in small_glances])
        if large_glance is not None:
            faces_ = large_detector([large_glance.image])[0]
            large_glance.set_detections(faces_)
            faces.extend(large_glance.get_detections())
        for glance, faces_ in zip(small_glances, small_glance_faces):
            glance.set_detections(faces_)
            faces.extend(glance.get_detections())

        keep = nms(
            boxes=torch.tensor([obj.bbox for obj in faces]).float()\
                if faces else torch.empty((0, 4), dtype=torch.float),
            scores=torch.tensor([obj.score for obj in faces]).float()\
                if faces else torch.empty((0,), dtype=torch.float),
            iou_threshold=0.5
        )
        faces = [faces[ind] for ind in keep]

        for face in faces:
            face.make_crop(frame)

        track_manager.update(faces)

        segmap = motion_glance_generator.get_last_segmap()
        segmap = cv2.resize(segmap, frame.shape[:2][::-1])
        segmap = np.tile(segmap[..., None], (1, 1, 3)).astype(np.uint8)

        out_frame = frame.copy()
        for face in faces:
            for pt in face.keypoints:
                pt = tuple(int(x) for x in pt)
                cv2.circle(out_frame, pt, 1, (0, 255, 0), 1)
        out_frame = np.concatenate([out_frame, segmap], axis=0)
        out_frame = cv2.cvtColor(out_frame, cv2.COLOR_RGB2BGR)
        video_writer.write(out_frame)

    frame_generator.release()
    video_writer.release()
